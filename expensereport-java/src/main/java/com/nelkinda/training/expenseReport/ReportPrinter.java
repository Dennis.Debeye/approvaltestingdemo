package com.nelkinda.training.expenseReport;

import com.nelkinda.training.model.ReportedExpense;
import org.apache.commons.text.TextStringBuilder;


public class ReportPrinter {

	private static final String FORMAT = "%-40s%s%n";

	public static String print(Report report) {
		TextStringBuilder textStringBuilder = new TextStringBuilder();
		textStringBuilder.appendln(FORMAT,"Expense Report created at:", report.getDate().toString());

		for (ReportedExpense expense : report.getExpenseList()){
			textStringBuilder.append(FORMAT,expense.getType().toString(), expense.getAmount() + (expense.isOverExpense()?"\tOVEREXPENSE":""));
		}

		textStringBuilder.appendln("-------------------");
		textStringBuilder.append(FORMAT,"Meal Expenses:", report.getMealExpenses());
		textStringBuilder.append(FORMAT, "Total Expenses:", report.getTotalExpenses());
		return textStringBuilder.build();
	}

}
