package com.nelkinda.training.expenseReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.nelkinda.training.model.ExpenseType;
import com.nelkinda.training.model.ReportedExpense;


public class Report {

	private final int BREAKFAST_EXPENSE_LIMIT = 1000;
	private final int DINNER_EXPENSE_LIMIT = 5000;
	private Date date;
	private List<ReportedExpense> expenseList;
	private int mealExpenses;
	private int totalExpenses;

	public Report() {
		this.date = new Date();
		this.expenseList = new ArrayList<>();
	}

	public void addExpense(ExpenseType type, int amount) {
		boolean isOverExpense = (type == ExpenseType.BREAKFAST && amount > BREAKFAST_EXPENSE_LIMIT)
				|| (type == ExpenseType.DINNER && amount > DINNER_EXPENSE_LIMIT);

		ReportedExpense reportedExpense = new ReportedExpense(type, amount, isOverExpense);
		totalExpenses += reportedExpense.getAmount();
		if (reportedExpense.getType() == ExpenseType.DINNER || reportedExpense.getType() == ExpenseType.BREAKFAST) {
			mealExpenses += reportedExpense.getAmount();
		}
		this.expenseList.add(reportedExpense);
	}

	public Date getDate() {
		return date;
	}

	public List<ReportedExpense> getExpenseList() {
		return expenseList;
	}

	public int getMealExpenses() {
		return mealExpenses;
	}

	public int getTotalExpenses() {
		return totalExpenses;
	}
}
