package com.nelkinda.training.expensePrint;

import java.util.Date;
import java.util.List;

import com.nelkinda.training.model.Expense;
import com.nelkinda.training.model.ExpenseType;
import org.apache.commons.text.TextStringBuilder;


public class ExpensePrinter {
	
	private static final String FORMAT = "%-40s%s%n";

	public static void main(String[] args) {
		List<Expense> expenses = List.of(
				new Expense(ExpenseType.DINNER, 200),
				new Expense(ExpenseType.BREAKFAST, 8_000),
				new Expense(ExpenseType.CAR_RENTAL, 20_000),
				new Expense(ExpenseType.DINNER, 1_000),
				new Expense(ExpenseType.BREAKFAST, 10_000),
				new Expense(ExpenseType.DINNER, 18)
		);
		System.out.println(ExpensePrinter.printReport(expenses));
	}

	public static String printReport(List<Expense> expenses) {
		int total = 0;
		int mealExpenses = 0;

		TextStringBuilder textStringBuilder = new TextStringBuilder();

		textStringBuilder.appendln("Expenses " + new Date());

		for (Expense expense : expenses) {
			if (expense.getType() == ExpenseType.DINNER || expense.getType() == ExpenseType.BREAKFAST) {
				mealExpenses += expense.getAmount();
			}

			String expenseName = "";
			switch (expense.getType()) {
				case DINNER:
					expenseName = "Dinner";
					break;
				case BREAKFAST:
					expenseName = "Breakfast";
					break;
				case CAR_RENTAL:
					expenseName = "Car Rental";
					break;
			}

			String mealOverExpensesMarker = null;

			if (expense.getType() == ExpenseType.DINNER && expense.getAmount() > 5000) {
				mealOverExpensesMarker = "X";
			} else if (expense.getType() == ExpenseType.BREAKFAST && expense.getAmount() > 1000) {
				mealOverExpensesMarker = "X";
			} else {
				mealOverExpensesMarker = " ";
			}

			textStringBuilder.append(FORMAT,expenseName, expense.getAmount() + "\t" + mealOverExpensesMarker);

			total += expense.getAmount();
		}
		textStringBuilder.appendln("-------------------");
		textStringBuilder.append(FORMAT,"Meal expenses:", mealExpenses);
		textStringBuilder.appendln(FORMAT,"Total expenses:", total);

		return textStringBuilder.build();
	}
}
