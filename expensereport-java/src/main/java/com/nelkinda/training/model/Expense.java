package com.nelkinda.training.model;

public class Expense {

	public Expense(ExpenseType type, int amount) {
		this.type = type;
		this.amount = amount;
	}

	ExpenseType type;
	int amount;

	public ExpenseType getType() {
		return type;
	}

	public void setType(ExpenseType type) {
		this.type = type;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
