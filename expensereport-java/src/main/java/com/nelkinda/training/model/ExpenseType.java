package com.nelkinda.training.model;

public enum ExpenseType {
	DINNER,
	BREAKFAST,
	CAR_RENTAL
}

