package com.nelkinda.training.model;

public class ReportedExpense extends Expense {
	boolean isOverExpense;

	public ReportedExpense(ExpenseType type, int amount, boolean isOverExpense) {
		super(type, amount);
		this.isOverExpense = isOverExpense;
	}

	public boolean isOverExpense() {
		return isOverExpense;
	}
}
