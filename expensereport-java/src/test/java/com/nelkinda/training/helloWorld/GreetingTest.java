package com.nelkinda.training.helloWorld;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;


class GreetingTest {
	@Test
	public void testHelloWorld(){
		//when
		String greeting = Greeting.sayHello("You");

		//then
		Approvals.verify(greeting);
	}
}
