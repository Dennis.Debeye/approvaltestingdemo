package com.nelkinda.training.expensePrint;

import java.util.List;

import com.nelkinda.training.expensePrint.ExpensePrinter;
import com.nelkinda.training.model.Expense;
import com.nelkinda.training.model.ExpenseType;
import org.approvaltests.Approvals;
import org.approvaltests.core.Options;
import org.approvaltests.scrubbers.DateScrubber;
import org.junit.jupiter.api.Test;


class SimpleExpensePrinterTest {

	@Test
	public void shouldApproveExpenseReport() {
		//when
		List<Expense> expenses = List.of(
				new Expense(ExpenseType.DINNER, 2)
		);

		//given
		String report = ExpensePrinter.printReport(expenses);

		//then
		Approvals.verify(report, new Options(DateScrubber.getScrubberFor("Wed Oct 18 11:20:56 CEST 2023")));
	}

}
