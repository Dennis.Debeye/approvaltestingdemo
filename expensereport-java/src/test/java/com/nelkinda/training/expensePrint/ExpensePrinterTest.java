package com.nelkinda.training.expensePrint;

import java.util.Collections;
import java.util.List;

import com.nelkinda.training.expensePrint.ExpensePrinter;
import com.nelkinda.training.model.Expense;
import com.nelkinda.training.model.ExpenseType;
import org.approvaltests.combinations.CombinationApprovals;
import org.approvaltests.core.Options;
import org.approvaltests.scrubbers.DateScrubber;
import org.junit.jupiter.api.Test;


class ExpensePrinterTest {

	@Test
	public void shouldApproveExpenseReport() {
		CombinationApprovals.verifyAllCombinations(
				this::callPrintReport,
				new ExpenseType[]{ExpenseType.DINNER, ExpenseType.BREAKFAST, ExpenseType.CAR_RENTAL},
				new Integer[]{10, 5001, 1001},
				new Options(DateScrubber.getScrubberFor("Wed Oct 18 11:20:56 CEST 2023"))
		);
	}

	private String callPrintReport(ExpenseType type, int amount) {
		List expenses = Collections.singletonList(new Expense(type, amount));
		return ExpensePrinter.printReport(expenses);

	}

}
