package com.nelkinda.training.expenseReport;

import com.nelkinda.training.expensePrint.ExpensePrinter;
import com.nelkinda.training.model.ExpenseType;
import org.apache.commons.text.TextStringBuilder;
import org.approvaltests.Approvals;
import org.approvaltests.core.Options;
import org.approvaltests.scrubbers.DateScrubber;
import org.junit.jupiter.api.Test;


class ReportTest {

	@Test
	public void addExpenseTest() {
		//when
		Report report = createReportWithExpenses();
		TextStringBuilder textStringBuilder = new TextStringBuilder();
		textStringBuilder.appendln(ReportPrinter.print(report));

		//given
		report.addExpense(ExpenseType.DINNER, 5000);
		textStringBuilder.appendln(">>>> Add Dinner for 5000 \n");

		//then
		textStringBuilder.appendln(ReportPrinter.print(report));
		Approvals.verify(textStringBuilder.build(), new Options(DateScrubber.getScrubberFor("Wed Oct 18 11:20:56 CEST 2023")));
	}

	private Report createReportWithExpenses() {
		Report report = new Report();
		report.addExpense(ExpenseType.DINNER, 1000);
		report.addExpense(ExpenseType.CAR_RENTAL, 1000);
		report.addExpense(ExpenseType.BREAKFAST, 1000);
		return report;
	}

}
